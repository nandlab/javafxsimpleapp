package com.example.calculator;

import com.example.frameworkworkag0906.SimpleButton;
import com.example.frameworkworkag0906.SimplePlugin;
import com.example.frameworkworkag0906.SimpleApplication;

import java.util.ArrayList;
import java.util.List;


public class Calculator extends SimplePlugin {
    @Override
    public String getTitle() {
        return "Calculator";
    }

    @Override
    public void initTextField() {
        getTextField().setText("2+3");
    }

    @Override
    public List<SimpleButton> getButtons() {
        ArrayList<SimpleButton> buttons = new ArrayList<>();
        buttons.add(new SimpleButton("Calculate", (e) -> getTextField().setText("5")));
        return buttons;
    }

    public static void main(String[] args) {
        SimpleApplication.setPlugin(new Calculator());
        SimpleApplication.launch(SimpleApplication.class, args);
    }
}
