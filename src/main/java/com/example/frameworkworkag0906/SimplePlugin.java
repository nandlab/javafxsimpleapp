package com.example.frameworkworkag0906;
import javafx.scene.control.TextField;

import java.util.List;

public abstract class SimplePlugin {
    private TextField textField;
    public SimplePlugin() {
        this.textField = null;
    }
    public TextField getTextField() {
        return textField;
    }
    public void setTextField(TextField textField) {
        this.textField = textField;
    }
    public abstract void initTextField();
    public abstract String getTitle();
    public abstract List<SimpleButton> getButtons();
}
