package com.example.frameworkworkag0906;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class SimpleApplication extends Application {
    static private SimplePlugin plugin;

    static public void setPlugin(SimplePlugin plugin) {
        SimpleApplication.plugin = plugin;
    }

    static public SimplePlugin getPlugin() {
        return plugin;
    }

    @Override
    public void start(Stage stage) throws IOException {
        TextField textField = new TextField();
        plugin.setTextField(textField);
        plugin.initTextField();
        HBox hbox = new HBox(textField);
        ArrayList<Button> buttons = new ArrayList<>();
        for (SimpleButton simpleButton : plugin.getButtons()) {
            Button button = new Button(simpleButton.getName());
            button.setOnAction(simpleButton.getActionHandler());
            buttons.add(button);
        }
        hbox.getChildren().addAll(buttons);
        Scene scene = new Scene(hbox);
        stage.setTitle(plugin.getTitle());
        stage.setScene(scene);
        stage.show();
    }
}
