package com.example.frameworkworkag0906;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SimpleButton {
    private final String name;
    private final EventHandler<ActionEvent> actionHandler;

    public String getName() {
        return name;
    }

    public EventHandler<ActionEvent> getActionHandler() {
        return actionHandler;
    }

    public SimpleButton(String name, EventHandler<ActionEvent> actionHandler) {
        this.name = name;
        this.actionHandler = actionHandler;
    }
}
